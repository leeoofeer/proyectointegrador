﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TheManager : MonoBehaviour
{
    public static TheManager theManager;
    bool gameStarted;

    void Awake()
    {
       if (!gameStarted)
        {
            theManager = this;
            SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);
            gameStarted = true;
        }
    }

    public void DescargarEscena(int escena)
    { StartCoroutine(Unload(escena)); }

    IEnumerator Unload(int escena) 
    {
        yield return null;
        SceneManager.UnloadScene(escena);
    }
    
}
