﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class BranMesh : MonoBehaviour
{

    public NavMeshAgent agent;
    
    private void OnTriggerStay(Collider other)
    {
        if (other.name == "Jugador")
            agent.SetDestination(other.transform.position);
    }
}
