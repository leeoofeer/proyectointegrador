﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveDiana : MonoBehaviour
{
    public GameObject dianaActivable;
    Material triggered;


    private void Start()
    {
        triggered = Resources.Load<Material>("ButtonPressed");
    }

    public void ActivarDiana()
    { dianaActivable.SetActive(true); }

    public void ChangeMaterial()
    { this.gameObject.GetComponent<Renderer>().material = triggered; }
}
