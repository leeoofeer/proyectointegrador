﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    public int vida = 100;
    VanishEffect _script;
    NavMeshAgent agent;
    public GameObject enemy2;
    bool estaMuerto;
    Vector3 diePosition;

    private void Start()
    {
        _script = this.transform.GetComponent<VanishEffect>();
        agent = GetComponent<NavMeshAgent>();

    }

    public void TakeDamage(int damage)
    {
        vida -= damage;
        if (vida <= 0 && !estaMuerto)
        {
            diePosition = transform.position;
            _script.ElEnemigoSeMuere();
            Debug.Log("El enemigo murio");
            StartCoroutine(SpawnMobs(transform.position));
            Debug.Log("El enemigo reaparecio");
            estaMuerto = true;
        }
    }


    private void OnTriggerStay(Collider other)
    {
        if (other.name == "Jugador")
        {
            agent.SetDestination(other.transform.position);

        }
    }

    void Update()
    {
        
    }



    IEnumerator SpawnMobs(Vector3 lugar)
    {
        yield return new WaitForSeconds(4);
        Instantiate(enemy2, lugar,Quaternion.identity) ;
        
    }
}
