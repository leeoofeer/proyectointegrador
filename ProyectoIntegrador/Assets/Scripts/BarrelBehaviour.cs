﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelBehaviour : MonoBehaviour
{
    public int health = 100;
    public int barrelName;
    public GameObject explodeEffect;
    public int radio = 1;

    void Start()
    {
        StartCoroutine(Desaparecer());
    }

    private void Update()
    {
       
        
    }


    void Explode(int damage)
    {
        //Effect
        Instantiate(explodeEffect, transform.position, transform.rotation);

        //Damage
        Collider[] colliders = Physics.OverlapSphere(transform.position, radio);
        foreach (Collider nearbyObject in colliders)
        {            

            EnemyBossManager BM = nearbyObject.GetComponent<EnemyBossManager>();
            if (BM != null)
            {
                BM.InflictDamage(damage);
            }

        }


        //Remove barrel
        Destroy(gameObject);
    }
        

    IEnumerator Desaparecer()
    {
        yield return new WaitForSeconds(5.5f);
        Destroy(explodeEffect);
        Destroy(gameObject);
    }

    public void RecibirDaño()
    {
        health -= 25;
        if (health <= 0 && barrelName == 3)
        {
            Explode(70);
        }
        else if (health <= 0 && barrelName == 2)
        {
            Explode(45);
        }
        else if (health <= 0 && barrelName == 1)
        {
            Explode(25);
        }
    }

}
