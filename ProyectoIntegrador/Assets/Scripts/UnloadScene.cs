﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UnloadScene : MonoBehaviour
{
    public int escena;
    bool descargado = false;
    public GameObject activable;
    public bool isTriggered = false;
   
    private void OnTriggerEnter(Collider other)
    {        
        DesargarEscena(escena);
        ActivarObjeto();
    }

    public void DesargarEscena(int escena)
    {
        if (!descargado)
        {
            descargado = true;
            TheManager.theManager.DescargarEscena(escena);
            Debug.Log("Escena descargada: " + escena);
            StartCoroutine(DestroyThis());
        }
    }

    void ActivarObjeto()
    {
        activable.SetActive(true);
    }
   

    //Esperar un frame para descargar la escena
    IEnumerator DestroyThis()
    {
        yield return null;
        Debug.Log("Unloader destruido");
        Destroy(this);        
    }
}
