﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class LoadLastTime : MonoBehaviour
{
    public Text thisText;
    GestorPersistencia miGestor;

    void Start()
    {
        miGestor = FindObjectOfType<GestorPersistencia>();
        StartCoroutine("LoadSaveData");
        Debug.Log(miGestor.data.lastTime);
    }

    IEnumerator LoadSaveData()
    {
        yield return new WaitForSeconds(1);
        thisText.text = "Ultimo tiempo tardado: " + miGestor.data.lastTime.ToString("F2") + "s";
    }    
}
