﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestruirEfecto : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SelfDestroy());
    }

    IEnumerator SelfDestroy()
    {
        yield return new WaitForSeconds(2f);
        Destroy(gameObject);
    }


}
