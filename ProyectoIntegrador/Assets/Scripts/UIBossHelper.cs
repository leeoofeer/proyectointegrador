﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIBossHelper : MonoBehaviour
{
    public Slider slider;

    void Awake()
    {
        slider = GetComponentInChildren<Slider>();
    }

    void Start()
    {
        slider.gameObject.SetActive(false);

    }

    public void SetUIHealthBarToActive()
    {
        slider.gameObject.SetActive(true);
        Debug.Log("Boss Bar activada");

    }


    public void SetUIHealthBarToInactive()
    {
        slider.gameObject.SetActive(false);
    }

    public void SetBossMaxHealth(int maxHealth)
    {
        slider.maxValue = maxHealth;
        slider.value = maxHealth;
    }

    public void SetBossCurrentHealth(int damage)
    {
        slider.value -= damage;
    }

}
