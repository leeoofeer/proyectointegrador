﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LoadScenes : MonoBehaviour
{
    public int escena;
    bool cargado;

    public void CargarEscena(int escena)
    {
        if (!cargado)
        {
            SceneManager.LoadSceneAsync(escena, LoadSceneMode.Additive);
            cargado = true;
        }
    }
}
