﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartBoss : MonoBehaviour
{
    public GameObject HPbar;
    public GameObject Dialog;
    private bool estaActivo = true;
    UIBossHelper bossHealthBar;
    private void Awake()
    {
        bossHealthBar = FindObjectOfType<UIBossHelper>();
    }

    void Update()
    {
        if (!estaActivo && Dialog.activeSelf == true)
        {
            HPbar.SetActive(true);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Boss iniciado");
        if(other.name == "Jugador")
        {
            Dialog.SetActive(true);
            StartCoroutine(EsperameUnSec());

            
        }
    }

    IEnumerator EsperameUnSec()
    {        
        yield return new WaitForSeconds(11.5f);
        estaActivo = false;
        bossHealthBar.SetUIHealthBarToActive();
    }

    

}
