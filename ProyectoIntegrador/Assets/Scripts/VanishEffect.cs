﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class VanishEffect : MonoBehaviour
{
    public Material baseMaterial;    
    public MeshRenderer thisMeshRenderer;
    float oldValue;
    public float vanishSpeed = 7f;
    private Collider thisCollider;
    private Rigidbody thisRigidbody;
    NavMeshAgent agent;
    public Collider secondCollider;

    void Awake()
    {
        thisCollider = GetComponent<Collider>();
        thisRigidbody = GetComponent<Rigidbody>();
        thisMeshRenderer = GetComponent<MeshRenderer>();
        agent = GetComponent<NavMeshAgent>();
        baseMaterial.SetFloat("Vector1_A4746487", 0f);
        thisMeshRenderer.material.SetFloat("Vector1_A4746487", 0.01f);
        
    }


    void Start()
    {
        thisMeshRenderer.material = new Material (thisMeshRenderer.material);       
        
    }      

    public void ElEnemigoSeMuere()
    {
        InvokeRepeating("Desaparecer", 0f, 0.09f);
        StartCoroutine(CancelInvoke());
    }


    void Desaparecer()
    {
        oldValue = thisMeshRenderer.material.GetFloat("Vector1_A4746487");
        thisMeshRenderer.material.SetFloat("Vector1_A4746487", oldValue + vanishSpeed * Time.deltaTime);
    }

    IEnumerator CancelInvoke()
    {
        thisRigidbody.isKinematic = true;
        thisCollider.enabled = false;
        agent.enabled = false;
        yield return new WaitForSeconds(2.5f);          //Debug.Log("Started Coroutine at timestamp : " + Time.time);
        CancelInvoke("Desaparecer");
        yield return new WaitForSeconds(1.6f);
        Destroy(gameObject);                            //Debug.Log("Finished Coroutine at timestamp : " + Time.time);
    }

}
