﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossBullet : MonoBehaviour
{
    public float movementSpeed = 1;
    HealthBarHandler HBR;


    private void Start()
    {
        StartCoroutine(SelfDestroy());
        HBR = FindObjectOfType<HealthBarHandler>();
    }


    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * movementSpeed);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Jugador")
        {
            HBR.TakeDamage(20);


            StartCoroutine(SelfDestroyCollision());

        }
    }

    IEnumerator SelfDestroy()
    {
        yield return new WaitForSeconds(4f);
        Destroy(gameObject);
    }
   
    IEnumerator SelfDestroyCollision()
    {
        yield return new WaitForSeconds(0.3f);
        Destroy(gameObject);
    }
}
