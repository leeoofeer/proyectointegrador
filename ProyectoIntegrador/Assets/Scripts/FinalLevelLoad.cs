﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalLevelLoad : MonoBehaviour
{
    UnloadScene unloadThis;
    LoadScenes loadFinal;

   

    void Start()
    {
        unloadThis = new UnloadScene();
        loadFinal = new LoadScenes();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Jugador")
        {
            loadFinal.CargarEscena(3);

            collision.transform.position = new Vector3(-0.2f, -98.93f, -74.1f);

            unloadThis.escena = 2;
            unloadThis.DesargarEscena(2);
            


        }

    }

}
