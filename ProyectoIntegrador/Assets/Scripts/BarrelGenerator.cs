﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelGenerator : MonoBehaviour
{
    public GameObject barrilAzul, barrilAmarillo, barrilRojo;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("GenerarBarril", 1f, Random.Range(5f, 10f));
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void GenerarBarril()
    {
        int r = Random.Range(0, 4);

        if (r == 3)
        {
            Instantiate(barrilRojo, transform.position, transform.rotation);
        }
        else if (r == 2)
        {
            Instantiate(barrilAmarillo, transform.position, transform.rotation);
        }
        else if (r == 1)
        {
            Instantiate(barrilAzul, transform.position, transform.rotation);
        }
        else { }
    }


}
