﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    Material triggered;
    Material matBase;
    public GameObject Menu_UI;
    public GameObject Jugador;
    public GameObject Hodor;
    float sensibilidad;
    public Slider mySlider;
    CameraController myCamController;
    Timer myTimer;

    private void Start()
    {
        Jugador = GameObject.Find("Jugador");
        triggered = Resources.Load<Material>("ButtonPressed");
        matBase = Resources.Load<Material>("ButtonIdle");
        myCamController = Jugador.GetComponent<CameraController>();
        sensibilidad = myCamController.sensibilidad;
        myTimer = FindObjectOfType<Timer>();
    }
     
    public void StartTimer()
    {
        myTimer.TimerTrigger();
    }
    public void BotonAudio()
    {
        Menu_UI.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
        myCamController.sensibilidad = 0;
    }

    public void BotonSalir()
    {
        UnityEditor.EditorApplication.isPlaying = false;
        Invoke("SalirJuego", 3);        
    }

    void SalirJuego()
    { Application.Quit(); }

    public void RemoveGO()
    { Hodor.SetActive(false); }

    public void ChangeMaterial()
    { this.gameObject.GetComponent<Renderer>().material = triggered; }

    public void ReturnMaterial()
    { this.gameObject.GetComponent<Renderer>().material = matBase; }

    public void SaveSound()
    { PlayerPrefs.SetFloat("MasterVolume", mySlider.value); Debug.Log("El Master volume tiene un valor de: " + PlayerPrefs.GetFloat("MasterVolume")); }

    public void LockearMouse()
    {
        Cursor.lockState = CursorLockMode.Locked;
        myCamController.sensibilidad = sensibilidad;
    }


}
