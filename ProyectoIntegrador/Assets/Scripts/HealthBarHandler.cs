﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;




public class HealthBarHandler : MonoBehaviour
{
    public Image circlerBar;
    public Image HorizontalBar;

    public float curHealth;
    public float maxHealth = 100;

    public float circlePercentage = 0.3f;

    private const float circleFillAmount = 0.75f;
    public GameObject damageScreen;

    void Start()
    {
        curHealth = maxHealth;
    }

    void Update()
    {
        CircleFill();
        HorizontalFill();        
    }

    public void CircleFill()
    {
        float healthPercetange = curHealth / maxHealth;
        float circleFill = healthPercetange / circlePercentage;
        circleFill *= circleFillAmount;
        circleFill = Mathf.Clamp(circleFill, 0, circleFillAmount);
        circlerBar.fillAmount = circleFill;
    }

    public void HorizontalFill()
    {
        float circleAmount = circlePercentage * maxHealth;
        float extraHealth = curHealth - circleAmount;
        float extraTotalHealth = maxHealth - circleAmount;
        
        
        float extraFill = extraHealth/extraTotalHealth;

        extraFill = Mathf.Clamp(extraFill, 0, 1);

        HorizontalBar.fillAmount = extraFill;
    }

    public void TakeDamage(int damage)
    {
        curHealth -= damage;
        damageScreen.SetActive(true);
        StartCoroutine(DamageScreen());
        if (curHealth <= 0)
        {
            SceneManager.LoadScene(4);
        }
    }

    IEnumerator DamageScreen()
    {
        yield return new WaitForSeconds(1f);
        damageScreen.SetActive(false);
        
    }


}
