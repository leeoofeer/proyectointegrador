﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Timer : MonoBehaviour
{
    public Text timerText;
    public float tiempo = 0.00f;
    bool startTime = false;
    public void Update()
    {

        
        if (startTime)
        {
            tiempo += Time.deltaTime;
            timerText.text =  "Tiempo transcurrido: "+ tiempo.ToString("F2")+"s";
        }
        
    }

    public void TimerTrigger()
    {
        if (!startTime)
        {
            startTime = true;
        }
        else if (startTime)
        {
            startTime = false;
        }
    }


}
