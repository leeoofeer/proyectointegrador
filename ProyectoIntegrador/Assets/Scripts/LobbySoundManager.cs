﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class LobbySoundManager : MonoBehaviour
{
    AudioSource lobbySound;
    public Slider soundBar;
    
    // Start is called before the first frame update
    void Start()
    {
        lobbySound = GetComponent<AudioSource>();
        lobbySound.volume = PlayerPrefs.GetFloat("MasterVolume");
        soundBar.value = PlayerPrefs.GetFloat("MasterVolume");
    }

    // Update is called once per frame
    void Update()
    {
        lobbySound.volume = soundBar.value;
    }
}
