﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class EnemyBossManager : MonoBehaviour
{
    UIBossHelper bossHealthBar;
    private int enemyHealth = 1000;
    VanishEffect _script;
    bool isDead = false;

    private void Awake()
    {
        bossHealthBar = FindObjectOfType<UIBossHelper>();
    }

    private void Start()
    {
        bossHealthBar.SetBossMaxHealth(enemyHealth);
        _script = this.transform.GetComponent<VanishEffect>();
    }

    public void InflictDamage(int damage)
    {
        bossHealthBar.SetBossCurrentHealth(damage);


        if (bossHealthBar.slider.value <= 0)
        {
            _script.ElEnemigoSeMuere();
           
            isDead = true;
            StartCoroutine(DestroyBoss());
            

            
            
        }
    }
    
    IEnumerator DestroyBoss()
    {
        yield return new WaitForSeconds(0.25f);
        //SceneManager.LoadScene(5);

        if (isDead)
        {
            gameObject.SetActive(false);
        }
    }

    
}
