﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenLevel : MonoBehaviour
{
    public GameObject diana1;
    public GameObject diana2;
    public GameObject diana3;
    public GameObject diana4;
    public GameObject puerta;
    public GameObject puerta2;
    GestorPersistencia guardarJuego;

    void Update()
    {
      if  (diana1.activeSelf && diana2.activeSelf && diana3.activeSelf && diana4.activeSelf)
        {
            puerta.SetActive(false);
            puerta2.SetActive(false);
            guardarJuego.GuardarDataPersistencia();
        }
    }
}
