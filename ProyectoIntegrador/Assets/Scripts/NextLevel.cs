﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextLevel : MonoBehaviour
{
    Timer myTimer;
    GestorPersistencia miGestor;

    private void Start()
    {
        myTimer = FindObjectOfType<Timer>();
        miGestor = FindObjectOfType<GestorPersistencia>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Jugador")
        {
            myTimer.TimerTrigger();
            miGestor.data.lastTime = myTimer.tiempo;
            miGestor.GuardarDataPersistencia();
        }
        
        
    }
}
