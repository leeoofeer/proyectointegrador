﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Camera CharacterCamera;
    Vector2 mouseMirar;
    Vector2 suavidadV;
    public float sensibilidad = 5.0f;
    public float suavizado = 2.0f;   

    void Update()
    {
        var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

        md = Vector2.Scale(md, new Vector2(sensibilidad * suavizado, sensibilidad * suavizado));

        suavidadV.x = Mathf.Lerp(suavidadV.x, md.x, 1f / suavizado);
        suavidadV.y = Mathf.Lerp(suavidadV.y, md.y, 1f / suavizado);

        mouseMirar += suavidadV;
        mouseMirar.y = Mathf.Clamp(mouseMirar.y, -70f, 70f);
        CharacterCamera.transform.localRotation = Quaternion.AngleAxis(-mouseMirar.y, Vector3.right);
        transform.localRotation = Quaternion.AngleAxis(mouseMirar.x, transform.up);
    }
        
}
