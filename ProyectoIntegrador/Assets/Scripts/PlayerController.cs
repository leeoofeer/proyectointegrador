﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayerController : MonoBehaviour
{
    public float rapidezDesplazamiento = 10.0f;
    public float sprint;
    bool cursorLocked;
    public Camera myCamera;
    public GameObject pauseMenu;
    public GameObject Jugador;
    CameraController myCamController;
    private float sensibilidad;
    bool isOnFloor;

    void Start()
    {
        sprint = 1;
        Cursor.lockState = CursorLockMode.Locked;
        cursorLocked = true;
        myCamController = Jugador.GetComponent<CameraController>();
        isOnFloor = true;
    }

    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento * sprint;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento * sprint;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;
               
        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        
        if (Input.GetKeyDown(KeyCode.P) && myCamController.sensibilidad > 0)
        {
            sensibilidad = myCamController.sensibilidad ;    
            Cursor.lockState = CursorLockMode.None; cursorLocked = false;
            pauseMenu.SetActive(true);
            myCamController.sensibilidad = 0;
        }
        else if (Input.GetKeyDown(KeyCode.P) && myCamController.sensibilidad <= 0)
        {
            Cursor.lockState = CursorLockMode.Locked; cursorLocked = true;
            pauseMenu.SetActive(false);
            myCamController.sensibilidad = 5.0f;
        }

        if (Input.GetKey(KeyCode.LeftShift))   { sprint = 1.5f; }        
        if (Input.GetKeyUp(KeyCode.LeftShift)) { sprint = 1; }
        if (Input.GetKeyUp(KeyCode.Space) && isOnFloor) { Jump(); }

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = myCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;

            if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 5)
            {
                Debug.Log("El rayo tocó al objeto: " + hit.collider.name);

                if (hit.collider.name.Substring(0, 5) == "Boton")
                {
                    GameObject objetoTocado = GameObject.Find(hit.transform.name);
                    MenuController menuController = (MenuController)objetoTocado.GetComponent(typeof(MenuController));
                    LoadScenes sceneLoader = (LoadScenes)objetoTocado.GetComponent(typeof(LoadScenes));

                    if (objetoTocado.name == "BotonPlay")
                    {
                        if (menuController != null) { sceneLoader.CargarEscena(2); menuController.ChangeMaterial(); menuController.RemoveGO(); menuController.StartTimer(); }
                    }
                    if (objetoTocado.name == "BotonAudio")
                    {
                        if (menuController != null) { menuController.BotonAudio(); menuController.ChangeMaterial(); }
                    }
                    if (objetoTocado.name == "BotonSalir")
                    {
                        if (menuController != null) { menuController.BotonSalir(); menuController.ChangeMaterial(); }
                    }
                    

                }
                if (hit.collider.name.Substring(0, 5) == "Diana")
                {
                    GameObject objetoTocado = GameObject.Find(hit.transform.name);                    
                    ActiveDiana _activeDiana = (ActiveDiana)objetoTocado.GetComponent(typeof(ActiveDiana));


                    if (objetoTocado.name == "DianaPrimera")
                    {
                        if (_activeDiana != null) { _activeDiana.ActivarDiana(); _activeDiana.ChangeMaterial(); }
                    }
                    if (objetoTocado.name == "DianaSegunda")
                    {
                        if (_activeDiana != null) { _activeDiana.ActivarDiana(); _activeDiana.ChangeMaterial(); }
                    }
                    if (objetoTocado.name == "DianaTercera")
                    {
                        if (_activeDiana != null) { _activeDiana.ActivarDiana(); _activeDiana.ChangeMaterial(); }
                    }
                    if (objetoTocado.name == "DianaCuarta")
                    {
                        if (_activeDiana != null) { _activeDiana.ActivarDiana(); _activeDiana.ChangeMaterial(); }
                    }
                }
                
            }

        }
    }

    public void Jump()
    {
        float yPosition = 2.3f;

        transform.Translate(new Vector3(0.0f, yPosition, 0.0f));
        isOnFloor = false;
        StartCoroutine(CantJump());
    }

    IEnumerator CantJump()
    {
        yield return new WaitForSeconds(0.6f);
        isOnFloor = true;

    }

}
