﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretMode : MonoBehaviour
{
    public GameObject target;
    private bool targetLocked;

    public GameObject cannon;

    public float fireTimer;
    private bool shootAvailable;

    public GameObject bullet;
    
    void Start()
    {
        shootAvailable = true;
    }

    void Update()
    {
        if (targetLocked)
        {
            cannon.transform.LookAt(target.transform);
            cannon.transform.Rotate(0, 0, 0);
            if (shootAvailable)
            {
                Shoot();
            }


        }
    }

    void Shoot()
    {
        Transform _bullet = Instantiate(bullet.transform,cannon.transform.position,Quaternion.identity);
        _bullet.transform.rotation = cannon.transform.rotation;
        shootAvailable = false;
        StartCoroutine(FireRate());
    }

    IEnumerator FireRate()
    {
        yield return new WaitForSeconds(fireTimer);
        shootAvailable = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.name == "Jugador")
        {
            target = other.gameObject;
            targetLocked = true;
        }
    }
}
