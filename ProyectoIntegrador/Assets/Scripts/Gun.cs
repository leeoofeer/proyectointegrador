﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public int damage = 25;
    public float range = 50f;
    public Camera gunCam;
    public ParticleSystem fogonazo;
    public GameObject efectoImpacto;

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }
    }


    void Shoot()
    {
        fogonazo.Play();
        RaycastHit hit;
        if (Physics.Raycast(gunCam.transform.position, gunCam.transform.forward, out hit,range))
        {
            Debug.Log("Hiteado  " + hit.collider.name);

            if (hit.collider.name.Substring(0, 4) == "Boss")
            {
                GameObject objetoTocado = GameObject.Find(hit.transform.name);
                EnemyBossManager BossManager = (EnemyBossManager)objetoTocado.GetComponent(typeof(EnemyBossManager));
                BossManager.InflictDamage(1);
            }
            else if (hit.collider.name.Substring(0, 6) == "Barrel")
            {
                GameObject objetoTocado = GameObject.Find(hit.transform.name);
                BarrelBehaviour barrelBehaviour = (BarrelBehaviour)objetoTocado.GetComponent(typeof(BarrelBehaviour));
                barrelBehaviour.RecibirDaño();
            }
            else
            {
                Enemy enemy = hit.transform.GetComponent<Enemy>();

                if (enemy != null) { enemy.TakeDamage(damage); }
            }

            

            GameObject impactoGO = Instantiate(efectoImpacto, hit.point, Quaternion.LookRotation(hit.point));
            Destroy(impactoGO, 1.6f);
        }

    }

}
